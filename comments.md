
My feedback for a piece on Nature Index about preprint 
===

# 1. When is the right time to publish a preprint?

## A. Is the earlier the better? 
Yes it is. I think at this point, there'ss no reason for us to postpone the publishing process. As long as we provide enough context in the background, attach the data, and the analysis as complete as possible, then I think we are ready to publish it as preprint.   

## B. Or should you get feedback from colleagues first? 
An iteration of feedback is always useful, but how about directly sharing the preprint's link to the community. We could always revise the preprint later on. With that said, I still believe we should wait for enough feedback before submission to journal. So my stance is to postpone the submission to journal, not the preprint.  

## C. If the paper is an early draft, is there a way to indicate that?
We could make a stamp on it by writing `1st draft/or initial draft` or simply by putting the date of edit on the document.

# 2. Where should you publish a preprint?

## A. I've seen a few influential COVID-19 preprints published on lab or project websites rather than posted to an official archive. Is that problematic? 
Hmmm first I would explain my habit on this question. I would setup a blogpost of Github/lab site to describe my idea (proposal), data or analysis, just to set my mind to make a sprint on it, to make an archive, and also to gain some attention. After a day or two, then I would reformat the blogpost  to something more formal using a generic outline of a paper. Then I would post it (I usually write with Google Docs for easy embedding) on my blog and upload it to a server for archiving purpose. This habit gives me a freedom to write my initial draft without worrying any strict guidelines or format, as well as gives a momentum to build my work and to have an searchable archive. Another reason is a cultural problem, especially in Indonesia, where people are still thinking of a blog as **JUST** a blog. Something that has no formal position in research evaluation. I am sure this differs in another country and culture. 

So yes it's still important to have a snapshot of your thoughts as preprint and upload it to a dedicated server.

## B. If you're choosing an archive, what should you consider?

1. Who's maintaining it. I would choose a non-profit server as my first selection, although I also notice other servers under for-profit entities.
2. The scope of the service. I would choose a specific scope of a server, but since there are not many choices for my field (geoscience), then I would choose a general preprint server.
3. The interface. I would choose a more simpler server with less box to fill in, but I also believe that the metadata of a document should be complete enough to make it searchable by common search engines.   

# 3. Should authors treat publishing a preprint any differently to submitting a paper to a journal?

No. I think they should put as much attention in posting preprint as they do in submitting a paper to a journal. Perhaps, they should put more attention and creativity when they're posting preprint. :)

## A. Do you have particular recommendations for the title and abstract? Is there anything else authors should pay special attention to before hitting submit?
If the server doesn't provide enough metadata to store the name of the targeted journal, I am suggesting author to insert this line in the abstract box, `This manuscript / preprint is  submitted to / under review in Journal of XXXX`. 

# 4. What should authors do after they submit a preprint?

## A. Where should authors look for commentary / feedback?
I think they should make a strong announcement in several communication channels, eg: intra-university mailing list, social media, Whatsapp group. I have several examples that sending the preprint privately directly to prospective individuals (like you did) often lead to feedback. Another reason of not getting feedback is because they didn't share the preprint in a creative way that would draw direct attention. I usually take out the most interesting figure or chart from the document and post it online on my social media followed by a somewhat intriguing statement and link to the preprint. I learn these things from late Jon Tennant :). Twitter is still my go-to social media to reach international attention. I tend to use Facebook for Indonesian audience.    

# 5. How should authors handle negative feedback?

## A. Do you have examples of particularly good practice or outcomes? 
Hmmm, this would depend on the personality and internal situation of an author when they receive the negative feedback. But I often see authors react badly over a negative feedback because they are over-estimate the content of their manuscript. I am not turning myself on bad reviews/feedback, but I think we could react in a more proper manner in receiving feedback if we have the same manner and motivation when we posted the preprint initially.   

## B. What are your thoughts on retracting a preprint?
I haven't had an experience to retract my own preprint, but I always suggest people to leave it there and add some short explanation why this preprint shouldn't be treated the same as others. Making a short public text file and upload it along with the preprint or posting it on personal blog is enough.   

# 6. How should authors manage different versions of the paper?

## A. If you update a paper, is there a way to indicate how it has changed from the pervious version? 
Most preprint servers (eg [OSF-based](osf.io/preprints), [Figshare](figshare.com), and [Zenodo](zenodo.org)) have a version control feature. But in case it doesn't available, I suggest author to indicate the date of edit in the filename (eg: `2020-05-13-manuscript.docx`).

## B. If the paper is published in a journal, is there a way to link back to earlier versions in a preprint archive?
Authors could insert the preprint status and the link in the "conflict of interest" or "acknowledgment" section if the journal doesn't provide a dedicated section to post preliminary version of the manuscript.

# 7. Any other comments? I'm sure there are many other issues that I haven't thought of!

I am suggesting your piece to make a strong message that posting preprint is a way for authors to practice their freedom in scientific publishing and to make their mark in knowledge sharing landscape. This is important because author might think that their part is over after publishing their work in an OA venue. This is not necessary true because not all journal suggest author to provide a [FAIR data](https://www.go-fair.org/fair-principles/) along with their published manuscript. In Indonesia's context, I often suggest authors to not only making the data open as separate xls/csv file, but also to making the citation data open. This is important because lots of Indonesian scientific literature are still in printed format or not having a proper metadata online. I suggest them to export the bibliography section as `bibtex` or `ris` file and post it as supplementary materials.   


